package com.example.annotations

import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.util.Log
import android.view.MotionEvent
import android.view.View
import androidx.annotation.ColorInt
import java.util.*
import kotlin.math.abs

class DrawingCanvas @JvmOverloads constructor(
    c: Context?,
    attrs: AttributeSet? = null,
    defStyle: Int = 0
) :
    View(c, attrs, defStyle) {
    var imageBitmap: Bitmap? = null
        private set
    private var mOriginBitmap: Bitmap? = null
    private var mCanvas: Canvas? = null
    private var mPath: Path? = null
    private var mBitmapPaint: Paint? = null
    private var mPaint: Paint? = null
    private var mDrawMode = false
    private var mX = 0f
    private var mY = 0f
    private var mProportion = 0f
    private var savePath: LinkedList<DrawPath>? = null
    private var mLastDrawPath: DrawPath? = null
    private var mMatrix: Matrix? = null
    private var mPaintBarPenSize = 0f
    private var mPaintBarPenColor = 0
    var annotationsArray: MutableList<List<List<Double>>> = ArrayList(1000)
    var pointsArray: MutableList<List<Double>> = ArrayList(300)
    private fun init() {
        mBitmapPaint = Paint(Paint.ANTI_ALIAS_FLAG or Paint.DITHER_FLAG)
        mDrawMode = false
        savePath = LinkedList()
        mMatrix = Matrix()
    }

    fun drawPoints() {
        val points = arrayOf(
            arrayOf(
                doubleArrayOf(431.630859375, 259.96875),
                doubleArrayOf(431.630859375, 259.08984375),
                doubleArrayOf(431.0972900390625, 258.3226318359375),
                doubleArrayOf(425.6611022949219, 255.98095703125),
                doubleArrayOf(395.7677917480469, 252.827880859375),
                doubleArrayOf(356.43963623046875, 265.5931396484375),
                doubleArrayOf(318.7065734863281, 288.489013671875),
                doubleArrayOf(283.1179504394531, 324.5458984375),
                doubleArrayOf(257.0455017089844, 365.3602294921875),
                doubleArrayOf(243.54722595214844, 402.4732666015625),
                doubleArrayOf(239.398681640625, 420.142578125),
                doubleArrayOf(246.8037872314453, 510.3721923828125),
                doubleArrayOf(264.75799560546875, 538.0533447265625),
                doubleArrayOf(275.18096923828125, 550.54638671875),
                doubleArrayOf(296.5279846191406, 571.852783203125),
                doubleArrayOf(319.4919128417969, 588.13232421875),
                doubleArrayOf(355.8108215332031, 596.10546875),
                doubleArrayOf(409.7781982421875, 592.993408203125),
                doubleArrayOf(451.6915283203125, 582.8817138671875),
                doubleArrayOf(501.4378356933594, 556.590576171875),
                doubleArrayOf(545.1524047851562, 522.287841796875),
                doubleArrayOf(576.0137939453125, 482.591552734375),
                doubleArrayOf(591.7752685546875, 443.5283203125),
                doubleArrayOf(595.37109375, 400.814697265625),
                doubleArrayOf(587.7490234375, 357.48291015625),
                doubleArrayOf(577.6076049804688, 319.462646484375),
                doubleArrayOf(563.057373046875, 289.3228759765625),
                doubleArrayOf(540.6821899414062, 264.4520263671875),
                doubleArrayOf(510.79718017578125, 247.632568359375),
                doubleArrayOf(477.9264221191406, 241.8046875),
                doubleArrayOf(449.4233093261719, 241.69970703125),
                doubleArrayOf(427.8065490722656, 244.03466796875),
                doubleArrayOf(413.2004089355469, 246.9661865234375),
                doubleArrayOf(408.8485107421875, 249.77099609375),
                doubleArrayOf(409.482421875, 252.9375),
                doubleArrayOf(409.482421875, 252.9375)
            )
        )
        for (point in points) {
            for (j in point.indices) {
                var x = point[j][0].toFloat()
                var y = point[j][1].toFloat()
                val ht = measuredHeight.toFloat() / 607f
                val wh = measuredWidth.toFloat() / 1080f
                x *= wh
                y *= ht
                if (mProportion != 0f) {
                    x /= mProportion
                    y /= mProportion
                }
                if (j == 0) {
                    if (mLastDrawPath != null) {
                        mPaint?.color = mPaintBarPenColor
                        mPaint?.strokeWidth = mPaintBarPenSize
                    }
                    mPath = Path()
                    mPath?.reset()
                    mPath?.moveTo(x, y)
                    mX = x
                    mY = y
                } else {
                    mPath?.quadTo(mX, mY, (x + mX) / 2, (y + mY) / 2)
                    mX = x
                    mY = y
                }
            }
            if (mPath != null && mPaint != null) {
                mCanvas?.drawPath(mPath!!, mPaint!!)
                mPath?.lineTo(mX, mY)
                mCanvas?.drawPath(mPath!!, mPaint!!)
                mLastDrawPath = DrawPath(mPath, mPaint!!.color, mPaint!!.strokeWidth)
                mLastDrawPath?.let { savePath?.add(it) }
            }
            mPath = null
        }
        invalidate()
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
        var widthSize = MeasureSpec.getSize(widthMeasureSpec)
        var heightSize = MeasureSpec.getSize(heightMeasureSpec)
        if (imageBitmap != null) {
            if (imageBitmap!!.height > heightSize && imageBitmap!!.height > imageBitmap!!.width) {
                widthSize = heightSize * imageBitmap!!.width / imageBitmap!!.height
            } else if (imageBitmap!!.width > widthSize && imageBitmap!!.width > imageBitmap!!.height) {
                heightSize = widthSize * imageBitmap!!.height / imageBitmap!!.width
            } else {
                heightSize = imageBitmap!!.height
                widthSize = imageBitmap!!.width
            }
        }
        Log.d(
            TAG,
            "heightSize: $heightSize widthSize: $widthSize"
        )
        setMeasuredDimension(widthSize, heightSize)
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        if (imageBitmap == null) {
            imageBitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888)
        }
        mCanvas = imageBitmap?.let { Canvas(it) }
        mCanvas?.drawColor(Color.TRANSPARENT)
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        imageBitmap?.let {
            val proportion = canvas.height.toFloat() / it.height
            if (proportion < 1) {
                mProportion = proportion
                mMatrix?.reset()
                mMatrix?.postScale(proportion, proportion)
                mMatrix?.postTranslate((canvas.width - it.width * proportion) / 2, 0f)
                mMatrix?.let { matrix -> canvas.drawBitmap(it, matrix, mBitmapPaint) }
            } else {
                mProportion = 0f
                canvas.drawBitmap(it, 0f, 0f, mBitmapPaint)
            }
        }
    }

    override fun onTouchEvent(event: MotionEvent): Boolean {
        if (!mDrawMode) {
            return false
        }
        val x: Float
        val y: Float
        val coordinates: MutableList<Double> = ArrayList(2)
        coordinates.add(event.x.toDouble())
        coordinates.add(event.y.toDouble())
        pointsArray.add(coordinates)
        if (mProportion != 0f) {
            x = event.x / mProportion
            y = event.y / mProportion
        } else {
            x = event.x
            y = event.y
        }
        when (event.action) {
            MotionEvent.ACTION_DOWN -> {
                // This happens when we undo a path
                if (mLastDrawPath != null) {
                    mPaint?.color = mPaintBarPenColor
                    mPaint?.strokeWidth = mPaintBarPenSize
                }
                mPath = Path()
                mPath?.reset()
                mPath?.moveTo(x, y)
                mX = x
                mY = y
                if (mPath != null && mPaint != null) {
                    mCanvas?.drawPath(mPath!!, mPaint!!)
                }
            }
            MotionEvent.ACTION_MOVE -> {
                val dx = abs(x - mX)
                val dy = abs(y - mY)
                if (dx >= TOUCH_TOLERANCE || dy >= TOUCH_TOLERANCE) {
                    mPath?.quadTo(mX, mY, (x + mX) / 2, (y + mY) / 2)
                    mX = x
                    mY = y
                }
                if (mPath != null && mPaint != null) {
                    mCanvas?.drawPath(mPath!!, mPaint!!)
                }
            }
            MotionEvent.ACTION_UP -> {
                mPath?.lineTo(mX, mY)
                if (mPath != null && mPaint != null) {
                    mCanvas?.drawPath(mPath!!, mPaint!!)
                    mLastDrawPath = DrawPath(mPath, mPaint!!.color, mPaint!!.strokeWidth)
                }
                mLastDrawPath?.let { savePath?.add(it) }
                annotationsArray.add(pointsArray)
                Log.d(TAG, annotationsArray.toString())
                Log.d(TAG, coordinates.toString())
                pointsArray.clear()
                mPath = null
            }
            else -> {
            }
        }
        invalidate()
        return true
    }

    fun initializePen() {
        mDrawMode = true
        mPaint = null
        mPaint = Paint()
        mPaint?.isAntiAlias = true
        mPaint?.isDither = true
        mPaint?.isFilterBitmap = true
        mPaint?.style = Paint.Style.STROKE
        mPaint?.strokeJoin = Paint.Join.ROUND
        mPaint?.strokeCap = Paint.Cap.ROUND
        mPaint?.xfermode = PorterDuffXfermode(PorterDuff.Mode.SRC_OVER)
    }

    override fun setBackgroundColor(color: Int) {
        mCanvas?.drawColor(color)
        super.setBackgroundColor(color)
    }

    var penSize: Float
        get() = mPaint?.strokeWidth ?: 30f
        set(size) {
            mPaintBarPenSize = size
            mPaint?.strokeWidth = size
        }

    @get:ColorInt
    var penColor: Int
        get() = mPaint?.color ?: context.getColor(R.color.red)
        set(color) {
            mPaintBarPenColor = color
            mPaint?.color = color
        }

    fun loadImage(bitmap: Bitmap?) {
        mOriginBitmap = bitmap
        imageBitmap = bitmap?.copy(Bitmap.Config.ARGB_8888, true)
        mCanvas = imageBitmap?.let { Canvas(it) }
        invalidate()
    }

    fun undo() {
        Log.d(TAG, "undo: recall last path")
        if (!savePath.isNullOrEmpty()) {
            mCanvas?.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR)
            loadImage(mOriginBitmap)
            savePath?.removeLast()
            for (dp in savePath!!) {
                mPaint?.color = dp.paintColor
                mPaint?.strokeWidth = dp.paintWidth
                if (dp.path != null && mPaint != null) {
                    mCanvas?.drawPath(dp.path!!, mPaint!!)
                }
            }
            invalidate()
        }
    }

    private inner class DrawPath(
        var path: Path?,
        var paintColor: Int,
        var paintWidth: Float
    )

    companion object {
        private const val TAG = "Annotations"
        private const val TOUCH_TOLERANCE = 4f
    }

    init {
        init()
    }
}