package com.example.annotations

import android.graphics.BitmapFactory
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView


class MainActivity : AppCompatActivity() {
    private var mDrawingCanvas: DrawingCanvas? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setupViews()
        loadImage()
    }

    private fun setupViews() {
        mDrawingCanvas = findViewById(R.id.img)
        val mBrush: AppCompatImageView? = findViewById(R.id.brush)
        val mColorPanel: AppCompatImageView? = findViewById(R.id.color_panel)
        val mDraw: AppCompatTextView? = findViewById(R.id.draw_user_points)
        val mUndo: AppCompatImageView? = findViewById(R.id.undo)
        mBrush?.setOnClickListener {
            mBrush.setImageResource(if (BRUSH == 0) R.drawable.ic_brush else R.drawable.ic_pen)
            mDrawingCanvas?.penSize = if (BRUSH == 0) 70f else 30f
            BRUSH = 1 - BRUSH
        }
        mColorPanel?.setOnClickListener {
            mColorPanel.setImageResource(if (COLOR_PANEL == 0) R.drawable.ic_color_blue else R.drawable.ic_color_red)
            mDrawingCanvas?.penColor =
                (if (COLOR_PANEL == 0) getColor(R.color.teal_200) else getColor(R.color.red))
            COLOR_PANEL = 1 - COLOR_PANEL
        }
        mDraw?.setOnClickListener {
            mDrawingCanvas?.drawPoints()
        }
        mUndo?.setOnClickListener {
            mDrawingCanvas?.undo()
        }

        mDrawingCanvas?.initializePen()
        mDrawingCanvas?.penSize = 30f
        mDrawingCanvas?.penColor = getColor(R.color.red)
    }


    private fun loadImage() {
        val bitmap = BitmapFactory.decodeResource(resources, R.drawable.wp2)
        mDrawingCanvas?.loadImage(bitmap)
    }

    companion object {
        private var COLOR_PANEL = 0
        private var BRUSH = 0
    }
}